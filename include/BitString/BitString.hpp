#ifndef MESSAGE_H
#define MESSAGE_H

#include <bitset>
#include <cmath>
#include <random>



constexpr unsigned int n = 15; // Length of the frame


namespace std {

  class Devices
  {
    public:
      Devices(unsigned long k, double BER)
        : d1{1, static_cast<unsigned int>(pow(2, k)) - 1}
        , d2(1, static_cast<unsigned int>(1 / BER))
        , rd{}
        , e2{rd()}
      {}

      /**
       * @brief Get a random number in the range [1, 2^k - 1]
       *
       * @return unsigned int a random number
       */
      unsigned int GetD1() { return d1(e2); }

      /**
       * @brief Get a random number in the range [1, 1 / BER].
       *
       * @return unsigned int a random number
       */
      unsigned int GetD2() { return d2(e2); }

    private:
      uniform_int_distribution<unsigned int> d1{};
      uniform_int_distribution<unsigned int> d2{};

      random_device rd;
      mt19937 e2;
  };


  template<unsigned long Size>
  struct Functions
  {
    public:
      Functions(unsigned long k, double BER)
        : dev{k, BER}
      {}

      /**
       * @brief Get the index of the first 1 in bitstring str
       * + 1.
       *
       * @param str bitstring to check
       * @return unsigned long the size of str
       */
      unsigned long size(bitset<Size> const& str)
      {
        return str == bitset<Size>(0) ? 0 : static_cast<unsigned long>(floor(log2(str.to_ulong()))) + 1;
      }

      /**
       * @brief Generate a random message using the first
       * random distribution.
       *
       * @return bitset<Size>
       */
      bitset<Size> randomMessage()
      {
        return bitset<Size>(dev.GetD1());
      }

      /**
       * @brief Get the frame T to send based on data = msg
       * and P.
       *
       * @param msg Data to be sent
       * @param P The polynomial to run CRC algorithm
       * @return bitset<Size> frame to be sent over
       */
      bitset<Size> getT(bitset<Size> const& msg, bitset<Size> const& P, unsigned long sizeP)
      {
        bitset<Size> result = msg << sizeP - 1;

        for (unsigned long i = size(result) - 1; i >= sizeP - 1; --i) {
          if (result[i]) {
            result ^= P << (i - sizeP + 1);
          }
        }
        return result | (msg << sizeP - 1);
      }

      /**
       * @brief Add noise to the frame T based on a uniform distribution
       * in the range [1, 1 / BER]
       *
       * @param T Frame where the noise will be added
       * @return pair<bitset<Size>, bool> The noisy frame and the boolean showing if the
       *                                  it was changed.
       */
      pair<bitset<Size>, bool> addNoise(bitset<Size> T)
      {
        bool changed = false;

        for (int i = Size - 1; i >= 0; --i) {
          if (dev.GetD2() == 1) {
            T[static_cast<unsigned long>(i)] = T[static_cast<unsigned long>(i)] ? 0 : 1;
            changed = true;
          }
        }
        return make_pair(T, changed);
      }

      /**
       * @brief Check if the received frame T is
       * divided by P.
       *
       * @param T Frame to be checked.
       * @param P Polynomial used.
       * @return true Frame is received without errors
       * @return false Frame is received with errors
       */
      bool check(bitset<Size> T, bitset<Size> const& P, unsigned long sizeP)
      {
        for (unsigned long i = size(T) - 1; i >= sizeP - 1; --i) {
          if (T[i]) {
            T ^= P << (i - sizeP + 1);
          }
        }
        return T == bitset<Size>(0);
      }
    private:
      Devices dev;
  };
}


#endif // MESSAGE_H