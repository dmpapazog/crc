k = 10 \
T size = 15 [bits] \
Bit error rate = 0.001 \
Sample = 1 000 000 000 \
Expected % = 1.4895

|N|P|Changed [# msg]|Detected [# msg] |Changed %|Detected %|
|:--:|--:|--:|--:|:--:|:--:|
|1|110101|14 891 638|14 891 638|1.4892|1.4892|
|2|   101|14 892 973|14 845 061|1.4893|1.4845|

time taken for each sample: ~16m