#include <iostream>
#include <cmath>
#include <cstring>
#include "BitString/BitString.hpp"
#include <span>

using namespace std;
typedef unsigned long long ULL;

constexpr ULL SAMPLE_SIZE = 1'000'000UL;
constexpr unsigned long DATA_SIZE = 10UL;
constexpr double CONST_BER = 0.001;
constexpr int BASE_10 = 10;
constexpr int BASE_2 	= 2;

enum struct Mode : int
{
  defaults = 1,
  variableSampleSize = 2,
  user = 5,
  invalid
};

int tested = 0; // Millions of messages that where checked.
int j = 1;			// Counter that resets every 1 million.

/**
 * @brief Show how many messages have been checked.
 *
 */
void show();

int main(int argc, char const *argv[])
{
	auto mode = static_cast<Mode>(argc);

	unsigned long k = DATA_SIZE;			// (D) Message size to be sent
	double 			 BER = CONST_BER;			// Bit Error Rate
	bitset<n> 	 P = bitset<n>(string("110101")); 			// Polynomial to be used
	ULL 				 sample = SAMPLE_SIZE;	// Sample size

	span<char const*> view(argv, static_cast<long unsigned>(argc));
	switch (mode)
	{
		case Mode::defaults:
			break;
		case Mode::variableSampleSize:
			sample = strtoull(view[1], nullptr, BASE_10);
			break;
		case Mode::user:
			P 		 = bitset<n>(strtoul(view[1], nullptr, BASE_2));
			k 		 = strtoul (view[2], nullptr, BASE_10);
			BER 	 = strtod  (view[3], nullptr);
			sample = strtoull(view[4], nullptr, BASE_10);
			break;
		case Mode::invalid:
			cerr << "Wrong input\n";
			return 1;
	}

	Functions<n> funcs(k, BER);
	ULL changed  = 0; // Frames where noise is added.
	ULL detected = 0; // Noised frames that CRC detected.
	unsigned long sizeP = funcs.size(P);

	cout << "Sample size: " << sample << '\n';
	for (ULL i = 0; i < sample; ++i) {
		show();
		bitset<n> msg = funcs.randomMessage();

		auto 			T = funcs.getT(msg, P, sizeP);
		auto result = funcs.addNoise(T);

		if (result.second) {
			++changed;
		}
		if (!funcs.check(result.first, P, sizeP)) {
			++detected;
		}
	}
	cout << "\nChanged:  " << changed
			 << "\nDetected: " << detected << '\n';
	return 0;
}

void show()
{
	if (j == SAMPLE_SIZE) {
		cout << "\rTested: " << ++tested << " * 10e6" << flush;
		j = 1;
	} else {
		++j;
	}
}